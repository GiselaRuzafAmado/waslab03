var baseURI = "http://localhost:8080/waslab03";
var tweetsURI = baseURI+"/tweets";

var req;
var tweetBlock = "	<div id='tweet_{0}' class='wallitem'>\n\
	<div class='likes'>\n\
	<span class='numlikes'>{1}</span><br /> <span\n\
	class='plt'>people like this</span><br /> <br />\n\
	<button onclick='{5}Handler(\"{0}\")'>{5}</button>\n\
	<br />\n\
	</div>\n\
	<div class='item'>\n\
	<h4>\n\
	<em>{2}</em> on {4}\n\
	</h4>\n\
	<p>{3}</p>\n\
	</div>\n\
	</div>\n";

String.prototype.format = function() {
	var args = arguments;
	return this.replace(/{(\d+)}/g, function(match, number) { 
		return typeof args[number] != 'undefined'
			? args[number]
		: match
		;
	});
};

function likeHandler(tweetID) {
	var target = 'tweet_' + tweetID;
	var uri = tweetsURI+ "/" + tweetID +"/likes";
	// e.g. to like tweet #6 we call http://localhost:8080/waslab03/tweets/6/like
	
	req = new XMLHttpRequest();
	req.open('POST', uri, /*async*/true);
	req.onreadystatechange = function() {
		if (req.readyState == 4 && req.status == 200) {
			document.getElementById(target).getElementsByClassName("numlikes")[0].innerHTML = req.responseText;
		}
	};
	req.send(/*no params*/null);
}

function deleteHandler(tweetID) {
	req = new XMLHttpRequest();
	req.open('DELETE', tweetsURI + '/' + tweetID, /*async*/true);
	req.setRequestHeader('Authorization',localStorage.getItem(tweetID));
	req.onreadystatechange = function() {
		if (req.readyState == 4 && req.status == 200) {
			var element = document.getElementById("tweet_"+tweetID);
			element.parentNode.removeChild(element);
			localStorage.removeItem(tweetID);
		}
	};
	req.send(null);
}

function getTweetHTML(tweet, action) {  // action :== "like" xor "delete"
	var dat = new Date(tweet.date);
	var dd = dat.toDateString()+" @ "+dat.toLocaleTimeString();
	return tweetBlock.format(tweet.id, tweet.likes, tweet.author, tweet.text, dd, action);
}

function getTweets() {
	req = new XMLHttpRequest(); 
	req.open("GET", tweetsURI, true); 
	
	req.onreadystatechange = function() {
		if (req.readyState == 4 && req.status == 200) {
			var tweet_list = req.responseText;
			list = JSON.parse(tweet_list);
			var formated_tweets = []; 
			var parsedJSON = JSON.parse(tweet_list);
			
		       for (var i=0;i<parsedJSON.length;i++) {
		    	   if(localStorage.getItem(parsedJSON[i].id) === parsedJSON[i].token[0]) {
		    		   formated_tweets.push(getTweetHTML(parsedJSON[i], "delete"));
		    	   }
		    	   else formated_tweets.push(getTweetHTML(parsedJSON[i], "like"));
		         }
		       
			document.getElementById("tweet_list").innerHTML = formated_tweets;
		}
	};
	req.send(null); 
};

/*
 * 1) obtenir, via JSON.parse de req.responseText, el nou tweet nt que ha generat WallServlet.java; 
 * 2) cridar getTweetHTML(nt, "delete"); i,
 *  3), escriure l'html obtingut al principi de tot de la tweet_list.
 * */
function tweetHandler() {
	var author = document.getElementById("tweet_author").value;
	var text = document.getElementById("tweet_text").value;
	
	req = new XMLHttpRequest();
	req.open('POST', tweetsURI, /*async*/true);
	
	req.onreadystatechange = function() {
		if (req.readyState == 4 && req.status == 200) {
			var new_tweet = req.responseText;
			var nt = JSON.parse(new_tweet);
			localStorage.setItem(nt.id, nt.token[0]);
			document.getElementById("tweet_list").innerHTML = getTweetHTML(nt, "delete") + document.getElementById("tweet_list").innerHTML ;
		}
	};
	
	 req.setRequestHeader("Content-Type","application/json");
	 req.send(JSON.stringify({author: author, text: text}));

	document.getElementById("tweet_author").value = "";
	document.getElementById("tweet_text").value = "";

};

//main
function main() {
	document.getElementById("tweet_submit").onclick = tweetHandler;
	getTweets();
};
